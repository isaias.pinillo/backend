package controllers;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import service.UserService;
import util.RestResponse;

import java.io.IOException;

@RestController
public class UserController {

    @Autowired
    protected UserService userService;


    protected ObjectMapper mapper;

    @RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
    public RestResponse saveOrUpdate(@RequestBody String userJson){
        throw JsonParseException,JsonMappingException, IOException {
            User user = this.mapper.readValues(userJson, User.class);
            return null;
        }
        private boolean validate(User user){
            boolean isValid=true;

            if(user.getFirstName()==null){
                isValid = false;
            }
            if(user.getFirstSurname()==null){
                isValid = false;
            }
            if(user.getAddress()==null){
                isValid = false;
            }
            return  isValid;
        }
    }


}
